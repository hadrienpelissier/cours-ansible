---
title: 'QCM Ansible'
visible: true
---

##### Prénom : \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ 

##### Nom : \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_


#### Entourez la bonne réponse.


### Question 1

Pour configurer une machine, Ansible agit...

1. ... grâce à un agent depuis l'intérieur des machines à configurer.
1. ... de l'extérieur depuis le poste du DevOps ou un serveur d'administration.
1. ... depuis l'extérieur grâce à un orchestrateur comme Docker Swarm.


### Question 2

Pour utiliser un rôle tiers déjà installé il faut d'abord

1. Lancer `ansible install` qui se chargera de l'appliquer automatiquement sur les noeuds.
2. Lancer `ansible clone` puis utiliser r10k pour activer le rôle.
3.  Lire son mode d'emploi sur Internet (le README du projet GitHub par exemple) pour savoir quelles variables utiliser pour le paramétrer.

### Question 3
Quel est le format standard des fichiers playbook ?

1. yaml (.yml)
2. json (.json)
3. config (.cfg)

### Question 4

Ansible Galaxy désigne :

1.  Le dépôt officiel de rôles ansible et la commande pour les installer.
2. La communauté red hat orientée DevOps qui fournit des conseils sur `OpenShift`.
3. Le programme spatial de Red Hat pour concurrencer Elon Musk.

### Question 5

Quel module Ansible sert à installer des paquets sous Ubuntu ?

1. `package`
1.  `apt`
1. `debian_pkg`

### Question 6
Comment faire pour lister des serveurs de cloud dont les IP changent ?

1. Utiliser un inventaire statique
2. Utiliser un inventaire dynamique
3. Utiliser le module ping


### Question 7
Sur quels prérequis est basé Ansible ?

1. ssh et python
2. git et python
3. terraform et ssh

### Question 8
Qu'est-ce qu'un `handler` ?

1. Un post-traitement qui s'exécute seulement s'il a été appelé par
    une tâche précédente.
2. Un module spécial Ansible qui n'utilise pas Python.
3. Un outil pour faciliter le suivi du code Ansible avec git.


### Question 9
Quelle est la syntaxe pour utiliser une variable Ansible ?

1. {{ mavariable }}
2. [ mavariable ]
3. variable = {valeur}

### Question 10
Où peut-on utiliser cette syntaxe (question précédente) ?

1. Pour remplacer n'importe quelle valeur dans un fichier yaml ou dans un template jinja
2. Seulement dans la section vars:
3. Uniquement avec terraform
