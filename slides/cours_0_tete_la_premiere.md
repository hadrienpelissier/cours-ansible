---
title: Ansible 
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
<!--
.bottom-bar[
  {{title}}
]
-->

---

class: impact

# Ansible
## *Infra as Code, DevOps for real!*

---

class: impact



---

## Quel est votre parcours ?
## Vos attentes ?

- est-ce que vous écrivez des scripts bash ?
- est-ce que vous avez déjà fait de l'**infrastructure as code** ou des labs ?

>>>

# Qu'est-ce qu'ansible?

On va directement mettre en place l'environnement et télécharger un playbook d'exemple

---

# Le DevOps

---

# À propos du mot DevOps

- Quelqu'un ? Un hybride de dev et d'ops... 
--

- Une méthode ? InfraAsCode, continuous integration and delivery, containerisation.
--

- Une façon de penser ? Une culture
--

- Une façon de virer des adminsys...


---

class: impact

# **Informatique** plutôt que numérique

## Réancrer les programmes dans leur **réalité d'exécution**

---

## Machines ain't smart. You are !

## Comment leur dire correctement quoi faire ?

---

# Infrastructure As Code

## Arrêtons de faire de l'adminsys adhoc

---

# Avantages : du contrôle

- Versionner le code avec git
--

- Tester les instrastructure pour plus de fiabilité
--

- Facilite l'intégration et le déploiement continus
  = vélocité
  = plusieurs versions par jours !

---


# Ansible, *langage* de programmation d'infrastructure

---

# Ansible 

--

- **Simple à mettre en œuvre** :
    - agentless
    - basé sur Python et SSH (présent sur linux souvent par défaut)
--

- **Versatile !!!**
--

- Pour configurer un simple serveur de dev jusqu'aux gros clusters de centaines de machines.
--

- Idempotence et versioning git accessible
--

- Syntaxe faite pour être lisible
--

- De plus en plus répandu ! c'est la **méga hype** !
--

- Sécurité car seulement SSH

---

## Pour coder durant cette formation

- **VSCode** : un éditeur puissant que vous connaissez un peu
- **Git et Framagit** : créer un dépôt, des commits et pousser ça sur internet !

---

class: impact

# Ansible

---

# Ansible

## Un logiciel pour configurer des machines
--
  
- Installer des logiciels (apt install)
--

- Modifier des fichier de configuration (/etc/…)
--

- Contrôler les services qui tournent (systemctl...)
--

- Gérer les utilisateurs et les permissions sur les fichiers
--

- etc.

---

# Ansible en image

.col-9[![](img/ansible_overview.jpg)]

---

 # Instrastructure As Code

## Du code qui contrôle l'état d'un serveur

Un peu comme un script bash mais :
--

- **Descriptif** : on peut lire facilement l'**état actuel** de l'infra
--

- **Idempotent** : on peut rejouer le playbook **plusieurs fois** pour s'assurer de l'état
--

- Du coup : playbook = état actuel de l'infra
--

- On contrôle ce qui se passe
--

- Assez différent de l'administration système *ad hoc* (= improvisation)


---
## Comparaison avec le bash (ou l'admin sys python)

- L'objectif d'Ansible est assez semblable au script Bash :
  - automatiser l'administration système
  - simple à mettre en œuvre : il suffit d'un fichier texte
  - très générique : basés sur des dépendances omniprésente sur les serveurs

- Ansible est une language de plus haut niveau basé sur des modules qui rendent les opérations plus facilement automatisables.

- ce language est concu spécialement pour manipuler la configuration des machines et exécuter des opérations de façon prédictible et reproductible.

---

# Infrastructure As Code

## Avantages

- On peut multiplier les machines (une machine ou 100 machines identiques c'est pareil).
--

- Git ! Gérer les versions de l'infrastructure et collaborer facilement comme avec du code.
--

- Tests fonctionnels (pour éviter les régressions/bugs)
--

- Pas de surprise = possibilité d'agrandir les clusters sans souci !
---

# Prérequis pour utiliser Ansible (minimal)

 1. Pouvoir se connecter en SSH sur la machine : **obligatoire** pour démarrer !!!
 2. **Python** disponible sur la machine à configurer : **facultatif** car on peut l'installer avec ansible

---

# Ansible en image

.col-9[![](img/ansible_overview.jpg)]

---




## Les forces d'Ansible

### Lisibilité (subjectif)

<!-- Image d'une ligne de commande bash -->

### Maintenabilité ?

---

### Déclaratif et idempotent

---

# DevOps et cloud

- Infrastructure as a Service (commercial et logiciel)
    - Amazon Web Services, Azure, Google Cloud, DigitalOcean
    - en local : Vagrant ou votre propre serveur de conteneurs.

- Plateform as a Service
    - Heroku, cluster Kubernetes

- Software as a Service
    - N'importe quelle web app
    - Adobe Creative Cloud est un bon exemple.

---

## Infrastructure as Code

- Une façon de définir une infrastructure dans un fichier descriptif et ainsi de créer et de provisionner dynamiquement les machines.

Un mouvement d'informatique lié au DevOps et au cloud :
- Ansible
- en particulier Terraform
- Vagrant pour le dev en local

---

# Ansible et DevOps

## Exprimer l'infrastructure et la configuration de façon centralisée

- Versioning

- Testing (unitaire, d'intégration, tests fonctionnels, canary testing)

-

## Rapprocher la production logicielle et la gestion de l'infrastructure

- Rapprocher la configuration de dev et de production (+ staging)

- Assumer le côté imprévisible de l'informatique en ayant une approche expérimentale

- Aller vers de l'intégration et du déploiement continu et automatisé.


## Inconvénients d'Ansible

- Relativement lent par défaut (subjectif), mais accélérable et parallélisable.
<!-- - Windows ? -->


## Opérations complexes, Ansible comme un language de programmation

- variables,
- facts,
- jinja,
- conditionals…



>>>

# Plan de la formation

---

### I) Présentation d'Ansible autour d'un "vrai" playbook

  - Ansible face à l'adminsys ad-hoc et aux scripts bash : déclaratif, lisibilité, contrôle
  - YAML et jinja pour quelque chose de compact, lisible, et extensible
  - Un outil de devops ? infrastructure as code, qualité par une méthode expérimentale,
  - Ansible face à Puppet/Chef/SaltStack: simplicité, versatilité, python

---

### II) Premier playbook  !

  - Objectif: éditer sa conf bash...
  - hosts, tasks, handlers, vars
  - un exemple de module très utile : template
    - ansible-doc: utilisez les exemples
  - Configurer une VM, configuration ssh et sudo (vagrant user, become)

---

### III) Bonnes pratiques Ansible

  - Structuration et réutilisation avec les rôles
    - Variables, dynamic VS static, debugger &-vvv, les filtres jinja
  - Ansible Galaxy
  - Example: Wordpress, MySQL, Nginx
  - Keep things simple! Exemples de mauvaises pratiques
  - From Vagrant to the cloud !
    - retour sur la logique DevOps
    - informatique "expérimentale" à base de labs

---

### IV) Infrastructures multi-tiers et contexte containerisé

  - Complex inventories and dynamic inventories, facts, tags
  - Ansible Tower / AWX
  - Ansible as bootstraping tool for an orchestration plateform puppet/docker/k8s
  - Custom modules and molecule testing

>>>
