---
title: Ansible 
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
<!--
.bottom-bar[
  {{title}}
]
-->

---

# Ansible
## *Infra as Code, DevOps for real !*

---

# Ansible et l'idempotence

- Une commande ou un playbook Ansible doivent pouvoir être relancés plusieurs fois sans introduire de nouveauté dans le système.

- Normalement une tâche exécutée une deuxième fois ne devrait pas renvoyer `changed`

- C'est la caractéristique d'Ansible qui permet d'avoir confiance lorsqu'on lance un playbook et donc d'aller vite dans les opérations (ne pas faire de vérification manuelle à chaque fois).

---

# Ansible et l'orchestration

- Ansible se connecte à des groupes et sous-groupes de machines

- Cette fonctionnalité est pratique pour la configuration mais également pour lancer des commandes ponctuelles

- On parle d'orchestration car cela permet de jouer au chef d'orchestre entre plusieurs groupes de machine depuis une machine d'administration.

---

# Commandes ad-hoc

Une commande ad-hoc Ansible est une commande bash qui permet d'effectuer une action sur des machines à partir d'un module Ansible mais sans l'écrire dans un playbook yml.

### Syntaxe

```
ansible <groupe_machine> -m <module> -a <arguments>
```

### Exemples

```
ansible all -m shell -a "uname -a"
ansible swarm_nodes -m apt -m "name=docker-ce"
```

---

# Playbook et Roles

- La base d'Ansible consiste à décrire la configuration d'une machine dans des fichiers `yml` appelés playbooks.
- Cependant écrire seulement des playbooks mène souvent à du code peu organisé et difficile à réutiliser.
- Ansible propose des sortes de librairies appelées `roles` qui permettent d'extraire une partie générique et cohérente d'un play pour la ranger dans un projet de code à part (un dossier)
- Fonctionne comme les modules puppets en quelque sorte.

---

# Structure d'un role

```
wordpress
├── defaults
│   └── main.yml
├── files
│   └── wordpress.tar.gz
├── handlers
│   └── main.yml
├── templates
│   └── wp-config.php.j2
├── vars
│   └── main.yml
└── tasks
    └── main.yml
```

Chaque dossier correspond à une section de playbook et contient: des fichiers/templates, des listes de commandes ansible, des liste de variables.


---

# Ansible Galaxy

Il s'agit du dépôt officiel de rôles ansible.

- On y trouve des rôles pour installer plein de choses

- Plus ou moins officiels et réputés

- Testés, notés et évalués sur leur fiabilité/compatibilité

On installe de nouveaux rôles, soit manuellement (`git clone` du projet dans le dossier `roles`), soit avec :

```bash
ansible-galaxy install user.nom-role
```

---

# Inventaires statiques et dynamiques

Ansible fonctionne avec des inventaires qui contiennent :

- des listes de machines rangées par groupes
- les façons de s'y connecter
- des variables spécifiques qui permettent d'adapter la configuration à chaque machine/environnement.

Les inventaires statiques (classiques) sont des fichier `ini` ou `yml` qui décrivent **"en dur"** ces informations.

- Ils ne peuvent pas s'adapter à des machines qui changent régulièrement puisqu'il faut changer notamment l'adresse ip ou le nom de domaine à la main.

---

## Les inventaires statiques

- Une liste de toutes les machines
- Classées par catégories
- Avec des variables spécifiques pour leur configuration


```
[nodes]
node1 ansible_host=10.0.2.4 node_number=1
node2 ansible_host=10.0.2.5 node_number=2
node3 ansible_host=10.0.2.7 node_number=3
node4 ansible_host=10.0.2.8 node_number=4

[nodes:vars]
ansible_user=elk-admin
ansible_password=el4stic
ansible_become_password=el4stic% 
```

---

# Les inventaires dynamiques

- Sont des programmes qui génèrent à la volée un inventaire (liste de machine, groupes, variables) en JSON.

- Les inventaires dynamiques interrogent une API pour récupérer ces informations.

- Par exemple celle d'un provider de cloud, ou en s'interfaçant avec Terraform.



---

# Terraform

C'est un outil d'Infrastructure as Code qui sert à provisionner des ressources (VPS, IP…) auprès de divers fournisseurs de cloud (IaaS).

- Amazon Web Services
- Azure
- Google Cloud
- Digital Ocean
- Scaleway
- votre VMWare ESXi on-premise
- etc.

Il utilise un langage descriptif qui liste les ressources à créer et les liens entre elles.

---

# Terraform et le cloud mixte

- L'intérêt de Terraform est de pourvoir provisionner chez différents fournisseurs avec le même outil.

- Cela permet de créer des cloud mixtes et multi-providers (par exemple : 50% chez AWS et 50% on-premise)

- Cependant, comme les clouds fournissent des ressources différentes avec de nomenclatures et propriétés différentes, la description des ressources est variable selon le provider (on peut quand même se servir des fichiers d'inventaires générés)

---