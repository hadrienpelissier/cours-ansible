- Mettre interface host-only en premier
- Rajouter guide d'install VirtualBox 5.2
- Modifier TP3 pour reconstituer un rôle correct sous forme de challenge (scénariser)
- faire installer wordpress à la place de apache2
- faire installer  yunohost avec une liste d'app quand rôle ansible is mature enoguh
- Exercices avec le module command, registerhttps://superuser.com/questions/1084129/ansible-task-to-confirm-if-a-process-is-running
https://docs.ansible.com/ansible/latest/modules/command_module.html#command-module
- modifier app flask pour une app plus intéressante
- directement faire le fichier hosts en YAML ou en INI mais pas demander les deux (préférence pour INI)
- refonder entièrement TP3