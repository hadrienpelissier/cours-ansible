---
title: 'TP2 - Créer un rôle pour installer Flask et utiliser Vagrant'
visible: true
---

<!-- ## Git
Nous allons réutiliser Git avec le même projet créé dans le TP1. Nous allons apprendre comment créer une branche avec `git branch` et basculer dessus avec `git checkout <branche>`. Créez donc une nouvelle branche `tp2` pour le projet TP2 et basculez dessus.

Pour ne pas réécrire votre mot de passe Git à chaque fois, vous pouvez configurer le cache de credentials :
`git config credential.helper cache` -->

### Mettre en place son cluster avec VirtualBox


- Nous allons utiliser deux nouvelles VM Ubuntu Server node1 et node2 sur le même réseau. Installez-les comme au TP2. Vérifiez que vous possédez bien leur adresse IP. Si vous réutilisez celles du TP1 faites attention à bien désactiver le service apache2 (via une tâche supplémentaire Ansible par exemple).

<!-- - Vérifiez qu'avec SSH vous pouvez vous connecter à ces nodes. -->

<!-- - Observez les paramètres réseau des 2 VM créées avec Vagrant. Configurez la VM Ubuntu Desktop dans VirtualBox pour qu'elle utilise le même réseau.  -->
<!-- - Rebootez la VM d'Ubuntu Desktop et vérifiez que vous pouvez bien pinger les 2 machines "node" depuis celle-ci. -->
<!-- - Vérifiez que vous pouvez vous connecter en SSH depuis la VM Ubuntu Desktop. -->
- Dans la suite du TP, nous appellerons "hôte" la VM avec Ubuntu Desktop.


## Configurer et tester Ansible
<!-- Version git-free -->
- Créons un dossier TP2, dans lequel nous allons commencer un projet selon le modèle d'Ansible :
    - `ansible.cfg` : config de base d’Ansible	
    - `hosts.yml` : fichier d'**inventaire** qui liste les machines à configurer (en YAML cette fois)
    - `ping.yml` : playbook ansible pour tester si les machines sont joignables avec Ansible
    - `deploy_app.yml` : playbook à remplir avec les étapes d’installation dans la partie suivante

- Compléter le fichier `ansible.cfg` pour préciser le chemin (relatif)  du fichier d'inventaire qui est désormais `hosts.yml`.

- Compléter le fichier `hosts.yml` pour se connecter en SSH. Toutes les informations nécessaires sont dans le fichier `hosts.ini` du TP précédent. **Attention, cette fois-ci, il faut respecter la syntaxe du YAML !** : aidez-vous [du chapitre sur l'inventaire de la documentation Ansible](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) pour écrire le fichier `hosts.yml` correctement.
	- il faut l'adresse IP pour contacter chaque serveur
	- indiquez l'utilisateur configuré pour la connexion SSH sur chacune des machines.
- Recherchez [dans le chapitre sur l'inventaire de la documentation Ansible](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) comment compléter `hosts.yml` pour regrouper les deux machines dans un groupe `nodes`.

- Copiez ceci dans le fichier `ping.yml` :
```yaml
---
- hosts: all
  gather_facts: False
  become: yes

  tasks:
    - name: 'link python to enable ansible' 
      raw: 'sudo ln -fs /usr/bin/python3 /usr/bin/python'
    - ping: 
```

- Enfin, pingez toutes les machines en exécutant le fichier `ping.yml` avec la commande Ansible adéquate.


Ajouter un site web en Python (Flask)
-------------------------------------

1.  Récupérez le dossier **site_flask_deploy** sur le partage :
```
site_flask_deploy/
    - deploy_app.yml
    - config_templates /
    ----- nginx.conf.j2
    ----- app.service.j2
    - flask_app /
    ----- app.py
    ----- requirements.txt
```
    Sortez les éléments dans votre dosssier de travail directement.

1.  Modifiez la valeur des variables de configuration de l'application 
    (**username**, **app_name**).
2.  Sur quel groupe de machines s'installera l'application ? (section
    **hosts:**) Modifiez les premières lignes du fichier en fonction.
3.  Lancez le playbook **deploy_app.yml**. Ce playbook copie l'application web Python sur le serveur et configure nginx pour la servir en http.
4.  Si tout s'est bien passé vous devriez pouvoir accéder au site web avec l'adresse IP d'un des nœuds. Ajoutez **/health** après l'adresse IP dans le navigateur. Quel est le résultat ?

Observer et debugger le playbook
--------------------------------

<!-- 1.  Ouvrez le fichier **flask_app/app.py**. Remplacez **hello** du
    return par **<h1>Hello World !</h1>**. Rechargez la page web
    dans le navigateur. -->
<!-- 2.  Personnalisez le message affiché par la fonction **hello( )**. -->
<!-- 3.  Ajoutez une nouvelle « page » au site accessible grâce au chemin 
    **/page2**.-->
1.  En étudiant le playbook, trouvez le chemin du fichier « service » de
    l'application. Il s'agit du fichier destination (dest:) de la tâche
    **template systemd service config** (vous pouvez utiliser la fonction de recherche de VSCode pour trouver cette tâche).
1.  Connectez-vous en SSH sur un des serveurs où Flask est
    installé.
1.  Affichez avec `cat` le contenu du fichier service identifié à la question précédente. Comparez avec le fichier template **app.service.j2** . Quelles variables ont été utilisées ?
<!-- 1.  Quelle commande bash utiliser pour relancer le service de notre
    application flask ?
1.  Quelle est la tâche équivalente dans notre playbook Ansible ? -->


Transformer le playbook deploy_app en rôle Ansible
---------------------------------------------------

Un rôle est une méthode systématique et plus réutilisable pour organiser les tâches Ansible dans un dossier (plutôt que dans un seul fichier YAML).

1.  Retournons dans le fichier `deploy_app.yml`. La tâche **restart nginx** à la fin du fichier devrait plutôt être un handler. Ajoutez une section **handlers:** à la fin du fichier (alignée avec **tasks:**) puis déplacez cette tâche dans cette nouvelle section handlers.
2.  Ajoutez une ligne **notify: restart nginx** à la ligne 61 après les 4 lignes de la tâche **template nginx site config**. Cette ligne indique à Ansible de déclencher notre handler de redémarrage si (et seulement si) la config a été modifiée.
3.  Relancez le playbook deploy_app.yml pour tester. Si la tâche **template nginx site config** ne modifie rien (`ok`), le handler ne devrait pas se lancer. Dans le cas contraire où le handler se lancerait, vous verriez une tâche `running handler` à la fin.
4.  Nous allons découper le playbook deploy_app.yml en plusieurs fichiers rangés correctement dans un rôle appelé `flask`. Créez un nouveau playbook **flask.yml** avec dedans :
    ```yaml
    - hosts: flask_nodes
      roles:
      - flask
    ```
5.  Nous allons maintenant créer ce rôle. Dans le dossier **roles** ajoutez un dossier **flask** avec à l'intérieur un dossier **tasks**. Si vous exécutez **flask.yml** pour le moment il n'y a pas d'erreur mais rien ne se passe car le rôle est vide.
6.  Créez un fichier **main.yml** dans **tasks**. Copiez à l'intérieur de ce fichier la liste de tâches contenues dans **deploy_app.yml** (sans les sections **hosts** et **vars**). Inspirez-vous du rôle `elasticsearch/tasks/main.yml` pour la syntaxe.
7.  Exécutez **flask.yml**, quel est le problème ?
8.  Pour ranger les variables, nous allons créer un dossier **defaults** dans le rôle **flask** et un autre fichier **main.yml** à l'intérieur.
9.  Copiez les variables de la section **vars:** de **deploy_app.yml** dans ce fichier en vous inspirant du rôle `elasticsearch`, et plus précisément du fichier `elasticsearch/tasks/main.yml` pour la syntaxe. [Le rôle d'exemple pour Elasticsearch est à télécharger ici](https://framagit.org/hadrienpelissier/tp_ansible/raw/master/roles.zip).
10. De même, copiez votre handler *restart nginx* dans un fichier `flask/handlers/main.yml`
11. Relancez **flask.yml**. Si tout va bien, vérifiez que vous pouvez afficher la page dans un navigateur.

